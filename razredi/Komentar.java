package razredi;

import java.time.LocalDateTime;


public class Komentar {

    private int id;
    private int idIzdelka;
    private String vsebina;
    private static LocalDateTime timestamp;

    public Komentar(int id, int idIzdelka, String vsebina) {
        this.id = id;
        this.idIzdelka = idIzdelka;
        this.vsebina = vsebina;
    }
}

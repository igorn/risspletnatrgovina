package razredi;

import java.util.ArrayList;

public class Ser implements java.io.Serializable{
    public ArrayList<Izdelek> izdelki;

    public Ser(){}

    public Ser(ArrayList<Izdelek> izdelki) {
        this.izdelki = izdelki;
    }


    public ArrayList<Izdelek> getIzdelki() {
        return izdelki;
    }

    public void setIzdelki(ArrayList<Izdelek> izdelki) {
        this.izdelki = izdelki;
    }
}

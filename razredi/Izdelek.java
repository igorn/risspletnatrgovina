package razredi;

public class Izdelek implements java.io.Serializable {
	private int _id;
	private double _cena;
	private String _naziv;
	private String _opis;
	private int zaloga;
	private int _ocena;
	private Komentar[] _komentarji;

	public Izdelek(){}


	public Izdelek(int _id, double _cena, String _naziv, String _opis, int zaloga, int _ocena, Komentar[] _komentarji) {
		this._id = _id;
		this._cena = _cena;
		this._naziv = _naziv;
		this._opis = _opis;
		this.zaloga=zaloga;
		this._ocena = _ocena;
		this._komentarji = _komentarji;
	}

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public double get_cena() {
		return _cena;
	}

	public void set_cena(double _cena) {
		this._cena = _cena;
	}

	public String get_naziv() {
		return _naziv;
	}

	public void set_naziv(String _naziv) {
		this._naziv = _naziv;
	}

	public String get_opis() {
		return _opis;
	}

	public void set_opis(String _opis) {
		this._opis = _opis;
	}

	public int get_ocena() {
		return _ocena;
	}

	public void set_ocena(int _ocena) {
		this._ocena = _ocena;
	}

	public Komentar[] get_komentarji() {
		return _komentarji;
	}

	public void set_komentarji(Komentar[] _komentarji) {
		this._komentarji = _komentarji;
	}

	public int getZaloga() {
		return zaloga;
	}

	public void setZaloga(int zaloga) {
		this.zaloga = zaloga;
	}
}
package gui;

import javax.imageio.stream.ImageInputStream;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;

import razredi.Izdelek;
import razredi.Ser;

public class Gui {

    private JTextField nazivTF;
    private JTextField cenaTF;
    private JTextField opisTF;
    private JButton dodajButton;
    private JComboBox izdelkiCB;
    private JButton izbrisiButton;
    private JButton urediButton;
    private javax.swing.JPanel JPanel;
    private JTextField zalogaTF;
    private JButton shraniIzdelkeButton;
    private JButton napisiPorociloButton;
    private JButton naloziIzdelkeButton;
    private JTabbedPane jtab;
    private JComboBox uporabnikizdCB;
    private JSpinner zalogaSB;
    private JLabel dodajvkosaroL;
    private JButton dodajVKosaroButton;
    private JComboBox kosaraCB;

    static ArrayList<Izdelek> izdelki = new ArrayList<Izdelek>();


    public static void main(String[] args) {
        JFrame frame = new JFrame("Gui");
        frame.setContentPane(new Gui().jtab);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public Gui() {
        dodajButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Izdelek izdelek = new Izdelek();

                double cena = Double.parseDouble(cenaTF.getText());
                String naziv = nazivTF.getText();
                String opis = opisTF.getText();
                int zaloga=Integer.parseInt(zalogaTF.getText());

                izdelek.set_id(izdelki.size());
                izdelek.set_cena(cena);
                izdelek.set_naziv(naziv);
                izdelek.set_opis(opis);
                izdelek.setZaloga(zaloga);


                izdelki.add(izdelek);

                izdelkiCB.addItem(izdelek.get_naziv());
                uporabnikizdCB.addItem(izdelek.get_naziv());

                cenaTF.setText("");
                nazivTF.setText("");
                opisTF.setText("");
                zalogaTF.setText("");


            }
        });
        izbrisiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(int i=0; i<izdelki.size(); i++){
                    if(izdelki.get(i).get_naziv().equalsIgnoreCase(String.valueOf(izdelkiCB.getSelectedItem()))){

                        izdelkiCB.removeItem(izdelki.get(i).get_naziv());
                        izdelki.remove(i);

                    }

                }



            }
        });

        urediButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(int i=0; i<izdelki.size(); i++){
                    if(izdelki.get(i).get_naziv().equalsIgnoreCase(String.valueOf(izdelkiCB.getSelectedItem()))){

                        cenaTF.setText(String.valueOf(izdelki.get(i).get_cena()));
                        nazivTF.setText(izdelki.get(i).get_naziv());
                        opisTF.setText(izdelki.get(i).get_opis());
                        zalogaTF.setText(String.valueOf(izdelki.get(i).getZaloga()));

                        izdelkiCB.removeItem(izdelki.get(i).get_naziv());
                        izdelki.remove(i);


                    }
                }


            }
        });
        shraniIzdelkeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String filename = "file.ser";
                Ser shrani = new Ser(izdelki);


                // Serialization
                try
                {
                    //Saving of object in a file
                    FileOutputStream file = new FileOutputStream(filename);
                    ObjectOutputStream out = new ObjectOutputStream(file);

                    // Method for serialization of object
                    out.writeObject(shrani);

                    out.close();
                    file.close();

                    System.out.println("Object has been serialized");

                }

                catch(IOException ex)
                {
                    ex.printStackTrace();
                    System.out.println("IOException is caught");
                }
            }
        });
        naloziIzdelkeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Ser nalozi = null;
                String filename = "file.ser";
                try
                {
                    // Reading the object from a file
                    FileInputStream file = new FileInputStream(filename);
                    ObjectInputStream in = new ObjectInputStream(file);

                    // Method for deserialization of object
                    nalozi = (Ser)in.readObject();

                    in.close();
                    file.close();

                   izdelki=nalozi.getIzdelki();
                    for (int i=0; i<izdelki.size(); i++){
                        izdelkiCB.addItem(izdelki.get(i).get_naziv());
                        uporabnikizdCB.addItem(izdelki.get(i).get_naziv());

                    }


                }

                catch(IOException ex)
                {
                    ex.printStackTrace();
                    System.out.println("IOException is caught");
                }

                catch(ClassNotFoundException ex)
                {
                    System.out.println("ClassNotFoundException is caught");
                }
            }
        });
        napisiPorociloButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                try {
                    BufferedWriter writer = new BufferedWriter(new FileWriter("porocilo.txt"));



                    writer.write("Izdelki na voljo na spletni trgovini: ");
                    writer.newLine();
                    for (int i=0; i<izdelki.size(); i++){
                    writer.write(izdelki.get(i).get_id()+" Naziv: "+izdelki.get(i).get_naziv()
                            +" Cena: "+izdelki.get(i).get_cena()
                            +" Opis: "+izdelki.get(i).get_opis()
                            +" Zaloga: "+ izdelki.get(i).getZaloga()
                            );
                    writer.newLine();

                    }



                    writer.close();
                    System.out.println("Successfully wrote to the file.");
                } catch (IOException ex) {
                    System.out.println("An error occurred.");
                    ex.printStackTrace();
                }
            }
        });
        dodajVKosaroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                for(int i=0; i<izdelki.size(); i++) {
                    if (izdelki.get(i).get_naziv().equalsIgnoreCase(String.valueOf(uporabnikizdCB.getSelectedItem()))) {
                        if((Integer)zalogaSB.getValue()<=izdelki.get(i).getZaloga()){
                            kosaraCB.addItem(izdelki.get(i).get_naziv()+" Kolicina: "+(Integer)zalogaSB.getValue());
                            izdelki.get(i).setZaloga(izdelki.get(i).getZaloga()-(Integer)zalogaSB.getValue());
                            if(izdelki.get(i).getZaloga()==0){
                                uporabnikizdCB.removeItemAt(i);

                            }
                        }else{
                            dodajvkosaroL.setText("Izbrali ste preveliko kolicino tega izdelka");


                        }


                    }
                }


            }
        });
    }
}
